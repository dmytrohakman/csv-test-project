<?php

namespace Itransition\Tests\TaskCsvBundle\Service;

use Itransition\TaskCsvBundle\Service\ProductService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductServiceTest extends TestCase
{
    public function testReadUploadedFile(): void
    {
        $productService = new ProductService();
        $uploadedFileMock = $this->getUploadedFileMock();
        $fileGenerator = $productService->readUploadedFile($uploadedFileMock);

        $this->assertIsIterable($fileGenerator);
    }

    private function getUploadedFileMock()
    {
        $uploadedFileMock = $this->createMock(UploadedFile::class);

        $uploadedFileMock->expects($this->any())
            ->method('getRealPath')
            ->willReturn(__DIR__ . '/stock.csv');

        return $uploadedFileMock;
    }
}

<?php declare(strict_types=1);

namespace Itransition\TaskCsvBundle\Enum;

enum ProductEnum: int
{
    public const COLUMN_PRODUCT_CODE = 0;
    public const COLUMN_PRODUCT_NAME = 1;
    public const COLUMN_PRODUCT_DESC = 2;
    public const COLUMN_PRODUCT_STOCK = 3;
    public const COLUMN_PRODUCT_COST_IN_GB = 4;
    public const COLUMN_PRODUCT_DISCONTINUED = 5;
}
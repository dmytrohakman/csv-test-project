<?php declare(strict_types=1);

namespace Itransition\TaskCsvBundle\Enum;

enum ProductRulesEnum: int
{
    public const PRODUCT_COLUMN_COUNT = 6;
    public const MIN_PRODUCT_COST = 5;
    public const MIN_PRODUCT_STOCK = 10;
    public const MAX_PRODUCT_COST = 1000;
    public const DISCONTINUED = 'yes';
}

<?php declare(strict_types=1);

namespace Itransition\TaskCsvBundle\Entity;

use Itransition\TaskCsvBundle\Enum\ProductRulesEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class ProductData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     */
    private $intProductDataId;

    /**
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $strProductName;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $strProductDesc;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern = "/P[0-9]*$/i"
     * )
     *
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    private $strProductCode;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" : NULL}))
     */
    private $dtmAdded;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default" : NULL})
     */
    private $dtmDiscontinued;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": NULL})
     */
    private $stmTimestamp;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $stock;

    /**
     * @Assert\Regex(
     *     pattern = "/^[0-9]+$|^[0-9]+\.[0-9]+$/"
     * )
     *
     * @ORM\Column(type="decimal", precision=15, scale=2, nullable=true)
     */
    private $costInGbp;

    public function getIntProductDataId(): int
    {
        return $this->intProductDataId;
    }

    public function getStrProductName(): string
    {
        return $this->strProductName;
    }

    public function setStrProductName(string $strProductName): self
    {
        $this->strProductName = $strProductName;

        return $this;
    }

    public function getStrProductDesc(): string
    {
        return $this->strProductDesc;
    }

    public function setStrProductDesc(string $strProductDesc): self
    {
        $this->strProductDesc = $strProductDesc;

        return $this;
    }

    public function getStrProductCode(): string
    {
        return $this->strProductCode;
    }

    public function setStrProductCode(string $strProductCode): self
    {
        $this->strProductCode = $strProductCode;

        return $this;
    }

    public function getDtmAdded(): ?\DateTimeInterface
    {
        return $this->dtmAdded;
    }

    public function setDtmAdded(\DateTimeInterface $dtmAdded): self
    {
        $this->dtmAdded = $dtmAdded;

        return $this;
    }

    public function getDtmDiscontinued(): ?\DateTimeInterface
    {
        return $this->dtmDiscontinued;
    }

    public function setDtmDiscontinued(\DateTimeInterface $dtmDiscontinued): self
    {
        $this->dtmDiscontinued = $dtmDiscontinued;

        return $this;
    }

    public function getStmTimestamp(): ?int
    {
        return $this->stmTimestamp;
    }

    public function setStmTimestamp(?int $stmTimestamp): self
    {
        $this->stmTimestamp = $stmTimestamp;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(?int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getCostInGbp(): string
    {
        return $this->costInGbp;
    }

    public function setCostInGbp(string $costInGbp): self
    {
        $this->costInGbp = $costInGbp;

        return $this;
    }

    /**
     * @Assert\IsFalse()
     */
    public function isCostAndStockNotValid()
    {
        return
            (
                $this->getCostInGbp() < ProductRulesEnum::MIN_PRODUCT_COST
                && (int) $this->getStock() < ProductRulesEnum::MIN_PRODUCT_STOCK
            )
            || ($this->getCostInGbp() > ProductRulesEnum::MAX_PRODUCT_COST);
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->setStmTimestamp(time());
        $this->setDtmDiscontinued(new \DateTime());
        $this->setDtmAdded(new \DateTime());
    }
}

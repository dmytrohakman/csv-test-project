<?php declare(strict_types=1);

namespace Itransition\TaskCsvBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Itransition\TaskCsvBundle\Entity\ProductData;
use Itransition\TaskCsvBundle\Service\ProductService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[AsCommand(
    name: 'itransition:import-product',
    description: 'Import product data.',
    hidden: false,
    aliases: ['itransition:add-products']
)]
class ImportProductCommand extends Command
{
    private const TEST_MODE = 'test';
    private const FILE_EXTENSION = 'csv';
    private const FILE_PATH_ARGUMENT = 'filepath';
    private const MODE_ARGUMENT = 'mode';

    private EntityManagerInterface $entityManager;
    private ProductService $productService;

    public function __construct(EntityManagerInterface $entityManager, ProductService $productService) {
        $this->entityManager = $entityManager;
        $this->productService = $productService;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(self::FILE_PATH_ARGUMENT, InputArgument::REQUIRED, 'The path to file.');
        $this->addArgument(self::MODE_ARGUMENT, InputArgument::OPTIONAL, 'The mode `test` will not store data to DB.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filepath = $input->getArgument(self::FILE_PATH_ARGUMENT);
        $mode = $input->getArgument(self::MODE_ARGUMENT);
        $uploadedFile = new UploadedFile($filepath, 'product_data');

        if (0 !== strcasecmp($uploadedFile->getFileInfo()->getExtension(), self::FILE_EXTENSION)) {
            $output->writeln(sprintf(
                "<error>File has wrong extension! Expect '%s' but got '%s'!<error>",
                self::FILE_EXTENSION,
                $uploadedFile->getFileInfo()->getExtension()
            ));

            return Command::FAILURE;
        }

        /** @var ProductData $fileRow */
        foreach ($this->productService->readUploadedFile($uploadedFile) as $productData) {
            if ($mode !== self::TEST_MODE) {
                $this->entityManager->persist($productData);
            }
        }

        if ($mode !== self::TEST_MODE) {
            $this->entityManager->flush();
        }

        if ($mode === self::TEST_MODE) {
            $output->writeln("<info>Mode 'test' active data will not be stored!</info> \n");
        }

        if (!empty($this->productService->getNotImportedProducts())) {
            $output->writeln(sprintf(
                "<comment>Products %s were not imported!</comment>",
                implode(', ', $this->productService->getNotImportedProducts())
            ));
        }

        $output->writeln([
            sprintf(
                "<info>Amount of products were proceed %s!</info>",
                $this->productService->getCountOfProceedItems()
            ),
            sprintf(
                "<info>Amount of products were imported %s!</info>",
                $this->productService->getCountOfImportedItems()
            ),
            sprintf(
                "<error>Amount of products were not imported %s!</error>",
                $this->productService->getCountOfNotImportedItems()
            )
        ]);

        return Command::SUCCESS;
    }
}
<?php declare(strict_types=1);

namespace Itransition\TaskCsvBundle\Service;

use Itransition\TaskCsvBundle\Entity\ProductData;
use Itransition\TaskCsvBundle\Enum\ProductEnum;
use Itransition\TaskCsvBundle\Enum\ProductRulesEnum;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductService
{
    private array $fileHeaders = [];
    private array $notImportedProducts = [];

    private int $countOfProceedItems = 0;
    private int $countOfNotImportedItems = 0;
    private int $countOfImportedItems = 0;

    private ValidatorInterface $validation;

    public function __construct(ValidatorInterface $validation)
    {
        $this->validation = $validation;
    }

    /**
     * @param UploadedFile $file
     * @return iterable
     */
    public function readUploadedFile(UploadedFile $file): iterable
    {
        $fileResource = fopen($file->getRealPath(),'r');
        $this->fileHeaders = fgetcsv($fileResource) ?: [];

        while ($productRow = fgetcsv($fileResource)) {
            $this->countOfProceedItems++;
            $productCode = $productRow[ProductEnum::COLUMN_PRODUCT_CODE] ?? '';

            if ($this->isNotProductValid($productRow)) {
                $this->notImportedProducts[] = $productCode;
                $this->countOfNotImportedItems++;
                continue;
            }

            $stock = $productRow[ProductEnum::COLUMN_PRODUCT_STOCK] ?? '0';
            $costInGBP = $productRow[ProductEnum::COLUMN_PRODUCT_COST_IN_GB] ?? '0';

            $productData = (new ProductData())
                ->setStrProductCode($productCode)
                ->setStrProductName($productRow[ProductEnum::COLUMN_PRODUCT_NAME] ?? '')
                ->setStrProductDesc($productRow[ProductEnum::COLUMN_PRODUCT_DESC] ?? '')
                ->setStock((int) $stock)
                ->setCostInGbp($costInGBP);

            if ($this->validation->validate($productData)->count() > 0) {
                $this->notImportedProducts[] = $productCode;
                $this->countOfNotImportedItems++;
                continue;
            }

            $this->countOfImportedItems++;

            yield $productData;
        }
    }

    /**
     * @param string[] $productRow
     * @return bool
     */
    public function isNotProductValid(array $productRow): bool
    {
        $discontinued = $productRow[ProductEnum::COLUMN_PRODUCT_DISCONTINUED] ?? '';

        return count($productRow) > ProductRulesEnum::PRODUCT_COLUMN_COUNT
            || count($productRow) < ProductRulesEnum::PRODUCT_COLUMN_COUNT
            || $discontinued !== ProductRulesEnum::DISCONTINUED;
    }

    /**
     * @return string[]
     */
    public function getFileHeaders(): array
    {
        return $this->fileHeaders;
    }

    /**
     * @return string[]
     */
    public function getNotImportedProducts(): array
    {
        return $this->notImportedProducts;
    }

    /**
     * @return int
     */
    public function getCountOfProceedItems(): int
    {
        return $this->countOfProceedItems;
    }

    /**
     * @return int
     */
    public function getCountOfNotImportedItems(): int
    {
        return $this->countOfNotImportedItems;
    }

    /**
     * @return int
     */
    public function getCountOfImportedItems(): int
    {
        return $this->countOfImportedItems;
    }
}
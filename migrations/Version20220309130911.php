<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309130911 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product_data CHANGE cost_in_gbp cost_in_gbp NUMERIC(15, 2) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product_data CHANGE str_product_name str_product_name VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE str_product_desc str_product_desc VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE str_product_code str_product_code VARCHAR(10) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE cost_in_gbp cost_in_gbp NUMERIC(10, 0) DEFAULT NULL');
    }
}
